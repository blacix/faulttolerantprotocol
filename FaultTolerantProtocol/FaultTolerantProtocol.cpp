﻿#include "TestExecutor.h"

#include <iostream>

// the application takes test case input files as program arguments and executes each of them
int main(int argc, char *argv[])
{

	// number of passed test cases
	int numPassed = 0;
	// number of failed test cases
	int numFailed = 0;
	// number of erroneous test cases
	int numError = 0;

	for (int i = 1; i < argc; i++)
	{
		TestExecutor test(argv[i]);
		test.execute();

		switch (test.getResult())
		{
		case TestExecutor::PASSED:
			++numPassed;
			break;
		case TestExecutor::FAILED:
			++numFailed;
			break;
		case TestExecutor::ERROR:
			++numError;
			break;
		default:
			break;
		}
	}

	std::cout << "summary" << std::endl;
	std::cout << "executed: " << numPassed + numFailed + numError << std::endl;
	std::cout << "passed: " << numPassed << std::endl;
	std::cout << "failed: " << numFailed << std::endl;
	std::cout << "error: " << numError << std::endl << std::endl;;

	std::cout << "press enter to exit";
	getchar();
	
	return 0;
}
