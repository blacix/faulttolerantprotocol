﻿#include "TestExecutor.h"
#include "Utils.h"

#include <iostream>
#include <fstream>
#include <sstream>

// init variables to -1 to be able to detect parse errors
TestExecutor::TestExecutor(const char * inputFileName) :
	mTransmitSleepMs(-1),
	mToggleSleepMs(-1),
	mToggleCycles(-1),
	mResultCompare(-1),
	mPacketsExpected(-1),
	mPerformance(false),
	mEncodedPackets(-1),
	mInputParseError(true),
	mTestresult(ERROR),
	mInputFileName(inputFileName)
{
	// open the input file and iterate through it line by line
	std::ifstream inputFile(mInputFileName);
	if (inputFile.is_open())
	{
		while (!inputFile.eof())
		{
			// first line is title
			std::getline(inputFile, title);
			// if title contains "performance", it is a performance test
			mPerformance = title.find("performance") != std::string::npos;			

			// second line transmitThreadSleepMs linkToggleThreadSlpeepMs linkToggleCycles
			std::getline(inputFile, line);
			std::stringstream ss(line);
			if (line.length() > 0)
			{
				ss >> mTransmitSleepMs >> mToggleSleepMs >> mToggleCycles;
			}

			// next line is the expected result
			// first char is = or > meaning that the num of 
			// expected successfully transmitted packets are equal or less than the next character
			// which will be put to mPacketsExpected
			std::getline(inputFile, line);
			if (line.length() > 0)
			{
				ss.str(line);
				// resets fail bit, EOL
				ss.clear();
				ss >> mResultCompare >> mPacketsExpected;
			}

			// next line tells wether the packets are encoded or raw app layer packets
			// 0 - raw packet - payload from the protocol perspective
			// 1 - encoded packet - header + payload
			std::getline(inputFile, line);
			if (line.length() > 0)
			{
				ss.str(line);
				ss.clear();
				ss >> mEncodedPackets;
			}

			//last n lines are the packets to be transmitted
			while (!inputFile.eof())
			{
				std::getline(inputFile, line);
				if (line.length() > 0)
					packets.push_back(Utils::hexStringToByteArray(line));
			}
		}

		inputFile.close();
	}

	// check for parse errors
	mInputParseError = mTransmitSleepMs < 0 || mToggleSleepMs < 0 || mToggleCycles < 0
		|| !(mResultCompare == '=' || mResultCompare == '<')
		|| mPacketsExpected < 0 || mEncodedPackets < 0;
}


void TestExecutor::execute()
{
	// exit on parse error
	if (mInputParseError)
	{
		std::cerr << "failed to execute " << mInputFileName << std::endl;
		mTestresult = ERROR;
		return;
	}

	std::cout << "executing: " << title << std::endl;

	long long duration = 0;
	unsigned long long bytesTransmitted = 0;

	// ep1 sends data to ep2.
	// encoded packets are sent directly via the link leaving sender node out of the game.
	Endpoint ep1;
	Endpoint ep2;
	TestLink link(ep1, ep2, mTransmitSleepMs, mToggleSleepMs, mToggleCycles);

	// if this is a perfomnace test, the first message of from the input file is sent many times
	// so the input file can contain only 1 message and the performance will be effected less
	// by parsing huge input files
	if (mPerformance)
	{
		if (packets.size() > 0)
		{
			DynamicBuffer packet = packets[0];
			for (size_t i = 0; i < 1000000 / packet.size(); i++)
			{
				packets.push_back(packet);
			}
		}

		// to make it pass
		mPacketsExpected = packets.size();
	}


	long long timestampBefore = Utils::getCurrentTimestamp();
	for (DynamicBuffer packet : packets)
	{
		if (mEncodedPackets > 0)
		{
			bytesTransmitted += packet.size();
			link.transmit(packet, packet.size(), &ep1);
		}
		else
		{
            bytesTransmitted += packet.size() + HEADER_SIZE + FOOTER_SIZE;
			ep1.transmit(packet);
		}
	}
	duration = Utils::getCurrentTimestamp() - timestampBefore;

	// check results	
	if (mResultCompare == '=')
	{
		// we expect every packet to be transmitted
        mTestresult = (mPacketsExpected == static_cast<int>(ep2.getReceivedPackets().size()) ? PASSED : FAILED);
	}
	else // must be '<' checked in constrctor already
	{
		// we expect less transmitted packets than sent
		// mSuccessfulTransmissions must be int to be able to initialize it to -1 for error checking
		mTestresult = (static_cast<int>(ep2.getReceivedPackets().size()) < mPacketsExpected ? PASSED : FAILED);
	}

	std::cout << "packets sent: " << packets.size() << std::endl
		<< "packets received: " << ep2.getReceivedPackets().size() << std::endl
		<< "packets expected: " << mResultCompare << " " << mPacketsExpected << std::endl;

	if (mPerformance)
	{
		std::cout 
			<< "duration: " << duration << std::endl
			<< "bytes transmitted: " << bytesTransmitted << std::endl
			<< "throughput: " 
			<< (int)(bytesTransmitted / (duration / (double)1000)) << " bytes/s"
			<< std::endl;
	}
	std::cout << toString(mTestresult) << std::endl << std::endl;
}

std::string TestExecutor::toString(const TestResult result)
{
	switch (result)
	{
	case PASSED:
		return "PASSED";
	case FAILED:
		return "FAILED";
	case ERROR:
		return "ERROR";
	default:
		return "UNKNOWN";
	}
}
