﻿#include "ILink.h"

ILink::ILink():
	mIsUp(false)
{
}

void ILink::up()
{
	if (!mIsUp)
		mIsUp = true;
}



void ILink::down()
{
	if (mIsUp)
		mIsUp = false;
}
