﻿#ifndef _ENDPOINT_H
#define _ENDPOINT_H
// project includes
#include "ProtocolCodec.h"
#include "RingBuffer.h"

// lib includes
#include <vector>
//#include<shared_mutex> // this is cpp14
#include<mutex>

// predeclare ILink as Link and Enpoint "knows" each other
class ILink;

// class that represents an endpoint that is attached to a link.
// Its duty is to perform i/o over the link and uses ProtocolCodec to encode and decode packets.
// From the protocol perspective the unencoded packet is the payload
// (that can be a packet for the higher layers of the application).
class Endpoint
{
public:
	Endpoint();
	~Endpoint() = default;

	// Function that transmits payload (higher level packet) over the link.
	// It will encode the packet first then send the encoded packet over the link.
	void transmit(const DynamicBuffer& payload);
	// function that receives one byte from the link. This will try to decode the input buffer.
	void receive(const uint8_t byte);
	// attach this node to a link in order to be able to send data over it.
	void attach(ILink* link);
	// callback when link becomes available.
	void linkUp();
	// callback when link becomes unavailable.
	void linkDown();
	// returns the number of packets that were (tried to be) transmitted over the link
	unsigned long long getStartedTransmissions() const { return mStartedTransmissions; }
	// returns the buffer containing the received decoded packets.
	const std::vector<DynamicBuffer>& getReceivedPackets() const { return mReceivedPackets; }

	// clears the input buffer
	void inline clearInputBuffer() { mBufferIn.clear(); }

private:
	// ringbuffer to store incoming data from the link
	RingBuffer mBufferIn;
	// preallocated buffer for outgoing packets
	PacketBuffer mEncodedPacketOut;
	// the codec that "knows" the protocol
	ProtocolCodec mProtocolCodec;
	// buffer for decoded incoming packets
	std::vector<DynamicBuffer> mReceivedPackets;
	// the attached link
	ILink* mLink;
	// the number of the started transmissions
	unsigned long long mStartedTransmissions;
	// since linkUp/Down callbacks may be called from a different thread
	// critical sections are guarded with a mutex (input buffer)
	//std::shared_mutex mMutex;
	std::mutex mMutex;
};

#endif
