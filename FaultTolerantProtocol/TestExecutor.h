﻿#ifndef _TEST_EXECUTOR_H
#define _TEST_EXECUTOR_H

#include "TestLink.h"

#include <string>
#include <vector>

// class that interprets test case files and executes them.
class TestExecutor
{
public:

	// enum for test result
	enum TestResult { PASSED, FAILED, ERROR };

	// Constructs the test object. Takes a test case file names as input
	// parses the file and prepares the class for test execution
	TestExecutor(const char * inputFileName);
	~TestExecutor() = default;

	// executes this test case
	void execute();

	// helper method for displaying test results in human readable format.
	static std::string toString(const TestResult result);

	// returns the result of this test case
	inline TestResult getResult() const { return mTestresult; }

private:
	// TestLink configuration parameters. For more info, refer to TestLink.h
	int mTransmitSleepMs;
	int mToggleSleepMs;
	int mToggleCycles;
	// an operator sent and expected received packets.
	// if it is '=' than the number fo sent and received packets must be the same
	// if it '<' the number of received packets are less than the sent ones
	char mResultCompare;
	// expected number of successfully received and encoded packets for mResultCompare operator
	int mPacketsExpected;
	// e.g:mResultCompare='=' and mPacketsExpected=5 means we expect exactly 5 packets to be successfully received

	// true if this is a performance test
	bool mPerformance;

	// number of successfully encoded packets
	int mEncodedPackets;
	// true if there was an error during parsing the input
	bool mInputParseError;
	// holds the test result after execution
	TestResult mTestresult;
	// the name of the input test case file
	std::string mInputFileName;
	// helper variable for parsing the input
	std::string line;
	// variable that holds the title of a TC
	std::string title;
	// buffer for packets received and encoded successfully
	std::vector<DynamicBuffer> packets;
};

#endif
