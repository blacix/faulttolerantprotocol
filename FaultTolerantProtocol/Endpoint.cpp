﻿#include "Endpoint.h"
#include "ILink.h"

#include <iostream>
#include <sstream>

Endpoint::Endpoint() :
	mLink(nullptr),
	mStartedTransmissions(0)
{
}

void Endpoint::transmit(const DynamicBuffer& payload)
{
	// create lock since we change the input buffer
	//std::shared_lock<std::shared_mutex> sharedLock(mMutex); // cpp14
	std::lock_guard<std::mutex> lock(mMutex);
	// check if we can send any data
	if (mLink != nullptr && payload.size() > 0)
	{
		// encode the packet and send it over the link
		size_t size = mProtocolCodec.encode(payload, mEncodedPacketOut);
		if (size > 0)
		{
			++mStartedTransmissions;
			mLink->transmit(mEncodedPacketOut, size, this);
		}
	}
}

void Endpoint::receive(const uint8_t byte)
{
	// create lock since we change the input buffer
	std::lock_guard<std::mutex> lock(mMutex);
	// add the newly received byte to the buffer
	mBufferIn.push(byte);

	// try to extract packets
	size_t byteProcessed = mProtocolCodec.decode(mBufferIn, mReceivedPackets);

	// remove the bytes from the buffer that are already processed
	if (byteProcessed > 0)
	{
		mBufferIn.pop(byteProcessed);
	}
}

void Endpoint::attach(ILink* link)
{
	mLink = link;
}

void Endpoint::linkUp()
{
	// do nothing
}


void Endpoint::linkDown()
{
	// create lock since we change the input buffer
	std::lock_guard<std::mutex> lock(mMutex);
	// try to decode the buffer as a last resort on link down
	mProtocolCodec.decode(mBufferIn, mReceivedPackets);
	clearInputBuffer();
}
