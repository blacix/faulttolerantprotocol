﻿#ifndef _ILINK_H
#define _ILINK_H
// project includes
#include "Endpoint.h"

// abstract class that represents a link implementation towards the Endpoint class
class ILink
{
public:
	ILink();
	virtual ~ILink() = default;

	// callback on link up
	virtual void up();
	// callback on link down
	virtual void down();
	// virtual method that every link implementation must implement. this method sends the data
	// to the remote Endpoint. How it sends the data is up to the classes that implement this interface.
	virtual void transmit(const PacketBuffer& rawData, const size_t size, const Endpoint* sender) = 0;
	virtual void transmit(const DynamicBuffer& rawData, const size_t size, const Endpoint* sender) = 0;
	// returns the state of the link
	inline bool isLinkUp() const { return mIsUp; }

protected:
	// variable to keep track of the state of the link
	bool mIsUp;

private:

};

#endif
