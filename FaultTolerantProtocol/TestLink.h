﻿#ifndef _TESTLINK_H
#define _TESTLINK_H

#include "ILink.h"
#include "Endpoint.h"

#include <thread>

// A real link implementation for testing the protocol.
// It holds two endpoints and calls the appropriate methods on them to enable
// sending and receiving data between them.
// To simulate link outages TestLink spawns threads to send data and to toggle the link up and down.
class TestLink : public ILink
{
public:
	// constructs a TestLink with 2 enpoints and thread parameters
	TestLink(Endpoint& node1, Endpoint& node2,
		int transmitSleepMs = -1, int toggleSleepMs = -1, int toggleCycles = -1);
	// destructs objects, joins threads
	~TestLink();

	// virtual method implementations that transmits data between endpoints.
	// TestLink only sends data toward the remote enpoint if link is up, that is, 
	// it will only call the remote endpoints receive function when the link is up
	// while the sender enpoint still transmits, thus the link is lossy.
	void transmit(const DynamicBuffer& rawData, const size_t size, const Endpoint* sender) override;
	void transmit(const PacketBuffer& rawData, const size_t size, const Endpoint* sender);

	// virtual method implementations for simulating ILink callbacks.
	void up();
	void down();

private:
	// Endpoints are taken as references.
	Endpoint & mEndpoint1;
	Endpoint& mEndpoint2;
	// the thread that transmit the data will sleep for mTransmitSleepMs between sending bytes
	int mTransmitSleepMs;
	// the thread that toggles the link up and down will sleep for mToggleSleepMs between 2 toggles
	int mToggleSleepMs;
	// the thread that toggles the link up and down will execute mToggleCycles toggle cycles
	int mToggleCycles;
	// the standard thread for toggling the link
	std::thread mThread;
};

#endif
