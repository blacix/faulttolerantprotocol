﻿// project includes
#include "TestLink.h"
#include "Utils.h"

// lib includes
#include <iostream>

TestLink::TestLink(Endpoint& ep1, Endpoint& ep2, int transmitSleepMs, int toggleSleepMs, int toggleCycles) :
	mEndpoint1(ep1),
	mEndpoint2(ep2),
	mTransmitSleepMs(transmitSleepMs),
	mToggleSleepMs(toggleSleepMs),
	mToggleCycles(toggleCycles)
{
	mEndpoint1.attach(this);
	mEndpoint2.attach(this);
	
	// make the link up
	up();
	
	// start toggle thread only if the class is configured so
	if (mToggleCycles > 0)
	{
		mThread = std::thread([&]()->void
		{
			for (int i = 0; i < mToggleCycles; i++)
			{
				// sleep for the configured time than toggle the link state
				std::this_thread::sleep_for(std::chrono::milliseconds(mToggleSleepMs));
				if (isLinkUp())
					down();
				else
					up();
			}
		});
	}
}


TestLink::~TestLink()
{
	// if toggle thread was started, join it
	if (mThread.joinable())
		mThread.join();
}


void TestLink::up()
{
	// call base to update link state in ILink
	ILink::up();
	// notify endpints
	// for this purpose, the Observer pattern could be applied
	mEndpoint1.linkUp();
	mEndpoint2.linkUp();
}


void TestLink::down()
{
	// call base to update link state in ILink
	ILink::down();
	// notify endpints
	// notify endpints
	// for this purpose, the Observer pattern could be applied
	mEndpoint1.linkDown();
	mEndpoint2.linkDown();
}

void TestLink::transmit(const DynamicBuffer& encodedPacket, const size_t size, const Endpoint* sender)
{
	// create an std::function that transmits bytes from encodedPacket from the sender to the remote enpoint.
	// The function will be called from a thread if TestExecutor class is configured for lossy link simulation.
	// in order to simulate a real transport between to real endpoints with a small delay between bytes.
	auto transmitFunction = [&]()->void
	{
		// get target node based on sender
		Endpoint* targetNode = sender == &mEndpoint1 ? &mEndpoint2 : &mEndpoint1;
		for (size_t i = 0; i < size; ++i)
		{
			if (isLinkUp())
			{
				// ithe remote endpoints receive function will only be called when the link is up
				// while the sender enpoint still transmits, thus the link is lossy.
				targetNode->receive(encodedPacket[i]);
			}
			// sleep for the configured time
			if (mTransmitSleepMs > 0)
				std::this_thread::sleep_for(std::chrono::milliseconds(mTransmitSleepMs));

		}
	};

	// if the TestLink was configured to be lossy, spawn a thread that calls the transmitFunction
	if (mTransmitSleepMs > 0)
	{
		std::thread t(transmitFunction);
		t.join();
	}
	else
	{
		transmitFunction();
	}
}

void TestLink::transmit(const PacketBuffer& encodedPacket, const size_t size, const Endpoint* sender)
{
	DynamicBuffer buffer(std::begin(encodedPacket), std::end(encodedPacket));
	transmit(buffer, size, sender);
}
